#ifndef _SERVER_H_
#define _SERVER_H_


#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/epoll.h>
#include"db.h"
//#include<sys/wait.h>



#define SERVER_IP "192.168.0.4"
#define SERVER_PORT 10086
#define MSG_BUF_SIZE 1024
#define MAX_EVENTS 20

using namespace std;
class server{
	public:
		server( DB * dbp);
		void server_init();
		void server_start();
//		void server_close();
		 ~server();
	private:
		struct sockaddr_in serverAddr;
		int servFd;
		int ret;
		int connFd;
		int epFd;
		char msg[MSG_BUF_SIZE];
		 DB *dbp;
};
#endif
