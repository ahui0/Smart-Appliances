#include<iostream>
#include"server1.h"
#include"db.h"

using namespace std;

server::server( DB * dbp){
	this->dbp=dbp;
	cout<<"0000"<<endl;
	cout<<SERVER_PORT<<endl;
	cout<<SERVER_IP<<endl;
	serverAddr.sin_family = PF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
	servFd=0;
	connFd=0;
	ret=0;
	msg[MSG_BUF_SIZE]={0};
	epFd=0;
}
server::~server()
{
	delete [] msg;
	delete dbp;
}

void server::server_init(){
	cout<<"init server"<<endl;
	servFd=socket(PF_INET,SOCK_STREAM,0);
	if(servFd<0)
	{
		cout<<"socket error!"<<endl;
		
	}
	cout<<"socket ok!"<<endl;
	ret = bind(servFd,(struct sockaddr*)&serverAddr,sizeof(serverAddr));
	if(ret<0)
	{
		cout<<"bind error"<<endl;
		close(servFd);

	}
	else
	{
		cout<<"bind ok!"<<endl;
	}
	ret=listen(servFd,10);
	if(ret<0)
	{
		cout<<"listen error!"<<endl;
		close(servFd);
	}
	else
	{
		cout<<"listening..."<<endl;
	}

}
void server::server_start(){
	epFd=epoll_create(10);
	if(epFd==-1)
	{
		cout<<"epoll_create error!"<<endl;
	}
	struct epoll_event ev,events[MAX_EVENTS];
	ev.events=EPOLLIN;
	ev.data.fd=servFd;
	ret=epoll_ctl(epFd,EPOLL_CTL_ADD,servFd,&ev);
	if(ret==-1)
	{
		cout<<"epoll_ctl error"<<endl;
	}
	else
	{
		cout<<"epoll_ctl ok !"<<endl;
	}

	while(1)
	{
		ret=epoll_wait(epFd,events,MAX_EVENTS,-1);
		if(ret==-1)
		{
			cout<<"epoll_wait error"<<endl;
		}
		else
		{
			cout<<"epoll_wait ok!"<<endl;
		}
		for(int i=0;i<ret;i++)
		{
			if(events[i].data.fd == servFd)
			{
				cout<<"wait for client..."<<endl;
				connFd=accept(servFd,NULL,NULL);
				
				if(connFd<0)
				{
					cout<<"accept error"<<endl;
					close(servFd);
				}
				else
				{
					cout<<"accept ok!"<<endl;
				}
				ev.events=EPOLLIN;
				ev.data.fd=connFd;
				epoll_ctl(epFd,EPOLL_CTL_ADD,connFd,&ev);
			}
			else
			{
				connFd=events[i].data.fd;
			int led	=0;
				
				led=recv(connFd,msg,sizeof(msg),0);
				if(led<0)
				{
					cout<<"recv error!"<<endl;
					
					close(connFd);
					continue;
				}
				else if(0==led)
				{
					cout<<"clinet shutdown"<<endl;
					close(connFd);
					
					epoll_ctl(epFd,EPOLL_CTL_DEL,connFd,&events[i]);
					continue;
				}
				else
				{
				cout<<msg<<endl;
				}
				char *buf=NULL;
				char *sign=NULL;
				char *mess=NULL;
				buf=strdup(msg);
				sign=strsep(&buf,",");
				mess=buf;
				if(strcmp(sign,"register")==0)
				{
					dbp->user_register((const int)connFd,mess);
				
				//	wait(1000);
				}
				else if(strcmp(sign,"login")==0)
				{
					dbp->user_login((const int)connFd,mess);
					
				}

			}
		}

	}
	
		close(servFd);
}














